#!/bin/sh

# output error information
set -e

# substitute env variables and place it into default config file
envsubst < /etc/nginx/conf.d/default.conf.tpl > /etc/nginx/conf.d/default.conf

# run nginx in foreground in the docker container
nginx -g 'daemon off;'